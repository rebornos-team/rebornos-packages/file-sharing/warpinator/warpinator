# warpinator

Allows simple local network file sharing

http://packages.linuxmint.com/pool/backport/w/warpinator

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/file-sharing/warpinator/warpinator.git
```

Requires: **python-grpcio-tools** (from AUR, in our repository now)
